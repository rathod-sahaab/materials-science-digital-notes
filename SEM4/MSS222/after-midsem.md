# Powder metallurgy process
Powder is filled in the cavity, compressed and then sintered below melting point.
 
## Characterstics of particles
### Size
Size of particle is given by
```math
\boxed { size\ of\ particles = \frac {15.2} M}
```

### Shape
Assuming that particle is a **perfect sphere**, with area $A$ and volume $V$.
```math
$$ A = \pi D^2,\ \ V = \frac \pi 6 D^3 
\implies \frac A V  = \frac 6 D $$
```
Generally,
```math
\boxed{K_S  = \frac A V D}
```
Where $`K_S`$ is the **shape factor**
For,
- sphere $`K_S`$ = 6
- not sphere $`K_S`$ > 6

## Steps in PMP
```mermaid
graph TD;
	B[Powder prepraration]-->C[Blending/Mixing]
	C-->D[Compacting]
	D-->E[Sintering]
```
### Powder prepraration
- Atomization
- Chemical reaction
- Electrolysis

### Blending and mixing
#### Blending
If chemical composition is same and size is different.

#### Mixing
Same size different composition.

```mermaid
graph TD;
	Mixing-->Mechanical;
	Mixing-->Screw;
	Mixing-->Blending;
```

### Compacting
It is pressure arrangment
```mermaid
graph TD;
	A[Compacting]-->B[Die press]
	A-->C[Roll press]
```
Product formed is called **green compact** and its density is termed as **green density**.

_**Green:** Still a chance to achieve true density._

