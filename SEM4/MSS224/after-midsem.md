# Grain Boundaries
## Grain Structure
```mermaid
graph TD;
A[Nuclei Formation] --> B[Nuclei Growth]
B --> C[Crystal/Grain Structure]
```
![Grain Formation process image](https://practicalmaintenance.net/wp-content/uploads/Process-of-Crystallization1.jpg)

### Grain
Each crystal is called a grain.

_**Poly-crystaline material:** Composed of several crystals_
### Grain Boundaries
Boundaries between crystals.

- These are the regions of  **atomic disorder** and only a **few atomic diameters wide**.
- They act as obstacles to motion of dislocations.
- Greater the number of of grain boundaries, finer the grains, stronger the material. According to Hall Pecth effect:
```math
	\sigma = \sigma_0 + \frac K {\sqrt d}
```
- Grain Boundaries are responsible for grain growth on heating.
- Larger grain growth on expense of smaller ones.
- Orientation changes across grain boundaries.

![](https://lh3.googleusercontent.com/proxy/zDnVZi4DnqH8O2j4qCKVnAhCISAjep3NufJa-ucJU2TIZAMNWhJGP0kVZpf5_ofYelkpO0FF0WZSLOE0_B7uLR1nshH_J31NvFCaauSfN0CEScdlAOEB07M)

- Average number of nearest neighbours for an atom in the grain boundaries of closed packed structure will have co-ordination number <12 _i.e._ they are loosely bound as compared to bulk.
- They have higher free volume than the crystal.
- Species insoluble in the crystal segregate at grain boundaries.
- During phase transformation they are the preferred sites for nuceation.
- They have higher mobility, diffusivity, and chemical activity. _(#throwback: etching)_

## Accommodation of angular/linear misfits by structural dislocations
A crystal may not match with the adjacent one across the interface.
### Angular Misfits
Two crystals may be rotated with respect to each other.

### Linear Misfits
The Crystals may not have same lattice parameters _ex._ epitaxial films.

- Their can be combined of both of these.

Structural dislocations can accommodate
```mermaid
graph TD;
	A[structural dislocations] --> B[Tilt]
	A --> C[Twist]
	A --> D[Misfits]
```
